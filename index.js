/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */

request = require("request")
validator = require("validator")

const telegram = (token, method) =>
  `https://api.telegram.org/bot${token}/${method}`

const transform = (contact, name, message) => `${contact} (${name})\n\n${message}`

const sendToTelegram = (token, chat) => msg => {
  return {
    method: 'POST',
    json: true,
    url: telegram(token, 'sendMessage'),
    body: { chat_id: chat, text: msg }
  }
}

const send = (contact,name, message) => {
  const payload = transform(contact, name, message)

  return sendToTelegram(
    process.env.TELEGRAM_TOKEN,
    process.env.TELEGRAM_CHANNEL_CHAT_ID
  )(payload)
}

// TODO: Use express middleware to handle errors instead of returns
exports.onFormPost = (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');

  if (req.method === 'OPTIONS') {
    // Send response to OPTIONS requests
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.set('Access-Control-Max-Age', '3600');
    return res.status(204).end()
  } else {
    // Set CORS headers for the main request
    res.set('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-Type', 'application/json');

    if(validator.isEmpty(req.body.name || "", { ignore_whitespace: true })) {
      return res.status(400).json({error: "Invalid name"})
    }

    if(!validator.isEmail(req.body.email || "")) {
      return res.status(400).json({error: "Invalid e-mail address"})
    }

    if(validator.isEmpty(req.body.message || "", { ignore_whitespace: true })) {
      return res.status(400).json({error: "Invalid message"})
    }
    

    const handleError = (error) => {
      console.log("Failed Telegram POST")
      console.log(error)
      res.status(500).json({error: "Message transmission failure"})
    }

    const handleResponse = (response) => {
      console.log("Completed Telegram POST")
      console.log(response)
      res.status(200).json({})
    }

    request(send(req.body.email, req.body.name, req.body.message))
      .on('error', handleError)
      .on('response', handleResponse)
  }
}
